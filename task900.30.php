<?php
//Tạo hàm tinhTong($so1, $so2), trả về tổng của 2 biến số $so1, $so2
function tinhTong($so1, $so2){
    $bien1 = 1234;
    echo ($bien1);
    return $so1 + $so2;
}

// Tạo biến $bien1 = 123; và $bien2 = 456
    $bien1 = 123;
    $bien2 = 456;
// Gọi hàm trên, truyền vào 2 tham số $bien1 và $bien2 rồi hiển thị kết quả trên trình duyệt
    $bien3 = tinhTong($bien1, $bien2);
    echo ("<p/> Giá trị của biến \$bien1 là: {$bien1}</p>");
    echo ($bien3);
// Tạo hàm tinhTong2(), trong hàm này khai báo 02 biến global $bien1, $bien2. Tính và trả về tổng của 2 biến này. Gọi hàm và hiển thị kết quả trên trình duyệt

function tinhTong2(){
    //global $bien1, $bien2;
    return $bien1 + $bien2;
}
echo ("<br/>Kết quả khi gọi hàm tinhTong2 là: " .tinhTong2());