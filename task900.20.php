<?php
// Tạo biến $bien1 và gán giá trị ban đầu là 123
    $bien1 = 123;
// Tạo biến $bien2 và gán giá trị ban đầu là “456”
    $bien2 = "456";
// Tạo biến $bien3 = $bien1 + $bien2 và in giá trị $bien3 ra trình duyệt
    $bien3 = $bien1 + $bien2;
    echo ($bien3);
//Tạo biến $bien4 = $bien1 . $bien2 và in giá trị $bien4 ra trình duyệt
    $bien4 = $bien1 . $bien2;
    echo ("<br/>" . $bien4);
// Tạo biến mảng $bien5 = [1,4,6, “2”, “8”] và dùng hàm print_r để in giá trị biến mảng ra trình duyệt
    $bien5 = [1,4,6, "2", "8"];
    print_r($bien5);
// Tạo biến chuỗi $bien6 dạng heredoc, với nội dung là 1 đoạn mã HTML có chứa giá trị biến $bien4 rồi in giá trị biến chuỗi $bien6 ra trình duyệt
$bien6 = <<<HERE
    <h2>Giá trị lưu trong biến \$bien4 là: {$bien4} 
HERE;
    echo ($bien6);
?>